import json
from websockets.sync.client import connect

positions = [
    {
        "id": "AAA",
        "position": {"x": 0, "y": 0, "z": 0},
        "rotation": {"x": 0, "y": 0, "z": 0},
    },
    {
        "id": "BBB",
        "position": {"x": 0, "y": 0, "z": 0},
        "rotation": {"x": 0, "y": 0, "z": 0},
    },
    {
        "id": "CCC",
        "position": {"x": 0, "y": 0, "z": 0},
        "rotation": {"x": 0, "y": 0, "z": 0},
    },
]


def run(pos):
    with connect("ws://localhost:8080") as websocket:
        json_data = json.dumps(pos)
        websocket.send(json_data)
        message = websocket.recv()
        print(f"=={pos['id']}==")
        print(f"Received: {message}")


for pos in positions:
    run(pos)
