import json
from websockets.sync.client import connect


def test_connection():
    with connect("ws://localhost:8080") as websocket:
        pass


def test_communication():
    def send_data(data):
        with connect("ws://localhost:8080") as websocket:
            json_data = json.dumps(data)
            websocket.send(json_data)
            return websocket.recv()

    first_example_object = {
        "action": "POST",
        "id": "AAA",
        "position": {"x": 0, "y": 0, "z": 0},
        "rotation": {"x": 0, "y": 0, "z": 0},
        "timestamp": 0,
    }
    second_example_object = {
        "action": "POST",
        "id": "BBB",
        "position": {"x": 0, "y": 0, "z": 0},
        "rotation": {"x": 0, "y": 0, "z": 0},
        "timestamp": 1,
    }
    query_from_first = {"action": "GET", "id": "AAA"}

    assert send_data(first_example_object) == '{"result": "ok"}'
    assert send_data(second_example_object) == '{"result": "ok"}'
    assert json.loads(send_data(query_from_first))["objects"] == [
        {
            "position": {"x": 0, "y": 0, "z": 0},
            "rotation": {"x": 0, "y": 0, "z": 0},
            "timestamp": "1",
            "custom": "",
            "id": "BBB",
        }
    ]
