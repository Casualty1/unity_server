#!/usr/bin/env bash
docker rm -f backend_server
docker build --tag=backend_server .
docker run -p 8080:8080 -it --name=backend_server backend_server
