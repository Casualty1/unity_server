# Multiplayer server for Unity  

## installation  

### supported platforms  
- [x] any linux distribution with docker engine installed  

**windows is unsupported**

### install with docker  
1. [Install docker engine](https://docs.docker.com/engine/install/ubuntu/)
2. Download the repository  
3. Run:  
```bash  
cd unity_project  
./build-docker.sh  
```  
  

## usage  
The websocket server is running on port `8080`.  
Communication is done using JSON objects.
```json  
{
    "action": "<either GET or POST>",
    "id": "<any series of letters>",
    // if the action is POST
    "position": {"x": 0, "y": 1, "z": 0},
    "rotation": {"x": 0, "y": 1, "z": 0},
    "timestamp": 1684316539728
}
```

## troubleshooting
- Permission issues  
    1. add current user to the `docker` group  
    2. reboot the computer  
- Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?  
    1. run `systemctl start docker.service`
