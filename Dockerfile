FROM alpine:3.18
RUN apk update
RUN apk add --no-cache python3 redis py-pip

WORKDIR /app
COPY requirements.txt .
COPY app .
RUN python3 -m pip install -r requirements.txt

EXPOSE 8080
ENTRYPOINT ["sh", "./start.sh"]
