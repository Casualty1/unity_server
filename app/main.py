import asyncio
from websockets.server import serve
import json
from database import update_object, get_other_objects
from validate import validate_object
import logging
import signal, sys


def clean_quit(sig, frame):
    print("C-c detected")
    sys.exit(0)


logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)


async def accept_connections(websocket):
    async for message in websocket:
        # validate incoming data
        client_obj = json.loads(message)
        ok, err = validate_object(client_obj)
        if not ok:
            logging.warning(f"Received invalid data {err=}")
            error_msg = {"error": err}
            await websocket.send(json.dumps(error_msg))
            continue  # skip packet

        # determine what action to take
        action_type = client_obj["action"].upper()
        client_id = client_obj["id"]  # client_id is needed for both POST and GET
        if action_type == "POST":
            logging.info(
                f'POST -- ID: {client_id} | Position: {client_obj["position"]} '
            )
            # handle optional custom data
            if "custom" not in client_obj:
                client_obj["custom"] = ""
            await update_object(client_id, client_obj)
            await websocket.send('{"result": "ok"}')

        elif action_type == "GET":
            object_list = await get_other_objects(client_id)
            logging.info(
                f"GET -- ID: {client_id} | Response length: {len(object_list)}"
            )
            jsoned_dump = json.dumps({"objects": object_list})
            await websocket.send(jsoned_dump)


async def main():
    signal.signal(signal.SIGINT, clean_quit)
    try:
        print("[+] Running on port 8080")
        async with serve(accept_connections, "0.0.0.0", 8080):
            await asyncio.Future()
    except KeyboardInterrupt:
        clean_quit()


asyncio.run(main())
