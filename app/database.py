import redis
import json

db = redis.Redis(host="localhost", port=6379, decode_responses=True)


async def update_object(obj_id, obj):
    """
    Save object identified by obj_id to the database
    """
    global db
    db.hset(
        f"object:{obj_id}",
        mapping={
            "position": json.dumps(obj["position"]),
            "rotation": json.dumps(obj["rotation"]),
            "timestamp": obj["timestamp"],
            "custom": obj["custom"],
        },
    )


async def get_other_objects(obj_id):
    """
    Return a list of all objects except the one with given id
    """
    global db
    other_keys = list(
        filter(lambda x: x != f"object:{obj_id}", db.scan_iter("object:*"))
    )
    list_of_objects = []
    for key in other_keys:
        obj = db.hgetall(key)
        obj["id"] = key.removeprefix("object:")
        obj["position"] = json.loads(obj["position"])
        obj["rotation"] = json.loads(obj["rotation"])
        list_of_objects.append(obj)
    return list_of_objects
