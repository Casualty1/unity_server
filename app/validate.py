def validate_object(obj):
    """
    Validate the request object uploaded by the client
    This functions assumes that the object has already been loaded from JSON
    """
    always_needed = [("id", str), ("action", str)]
    needed_for_post = [("position", dict), ("rotation", dict), ("timestamp", int)]
    optional = [("custom", str)]
    allowed_keys = always_needed + needed_for_post + optional
    for key, typ in always_needed:
        if key not in obj:
            return False, f"obj[{key}] must always be provided but now it's missing"
        if type(obj[key]) is not typ:
            return False, f"{key} must be {typ.__name__}"

    action = obj["action"].upper()
    if action == "POST":
        for key, typ in needed_for_post:
            if key not in obj:
                return (
                    False,
                    f"obj[{key}] must be provided for action=POST but now it's missing",
                )

            if type(obj[key]) is not typ:
                return False, f"{key} must be {typ.__name__}"
    for optional_key, key_type in optional:
        if optional_key in obj and not type(obj[optional_key]) is key_type:
            return False, f"optional key {optional_key} must be a string"

    return True, ""
